source /opt/ros/melodic/setup.bash;
source devel/setup.bash;

gnome-terminal -e roscore;

# Sleep to wait for roscore to come online
sleep 2;

rosrun ros_odrive ros_odrive;
